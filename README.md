# pos-load-test

## Install 

```bash
$ pip install -r requirements.txt
```

## Run script

```bash
$ python scripts/load_testing.py
```
