from locust import HttpLocust, TaskSet

urls = {
    "index": "/index.php",
    "products": "/products.php",
    "users": "/users.php",
    "shopping": "/shopping.php"
}


def index(l):
    l.client.get(urls.get("index"))


def products(l):
    l.client.get(urls.get("products"))


def users(l):
    l.client.get(urls.get("users"))


def shopping(l):
    l.client.get(urls.get("shopping"))


class UserBehavior(TaskSet):
    tasks = { index: 1, products: 1, users: 1, shopping: 1 }


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 5000
    max_wait = 9000
